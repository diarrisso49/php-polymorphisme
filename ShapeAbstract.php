<?php

 abstract class ShapeAbstract {
    abstract public function calculateArea(): float|int;
 }