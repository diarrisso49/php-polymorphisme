<?php

require_once 'ShapeAbstract.php';
class Circle extends ShapeAbstract
{

    public function __construct(private $radius)
    {
    }

    public function calculateArea(): float|int
    {
        return pi() * $this->radius * $this->radius;
    }

}