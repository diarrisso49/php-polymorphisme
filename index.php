<?php

require_once 'Circle.php';
require_once 'Square.php';


// WITH Interface example
/*$circle = new Circle(5);
echo round($circle->calculateArea(), 2). PHP_EOL;

error_log('Circle area: ' . $circle->calculateArea());
$square = new Square(4);

error_log('Square area: ' . $square->calculateArea());
echo $square->calculateArea();*/

// WITH Abstract class example

function printArea(ShapeAbstract $shape) {
    echo "Area: " . $shape->calculateArea() . PHP_EOL;
}

// Creating instances of Circle and Square
$circle = new Circle(5);
$square = new Square(4);

// Using polymorphism to calculate and print the area
printArea($circle);  // Outputs the area of the circle
printArea($square);  // Outputs the area of the square