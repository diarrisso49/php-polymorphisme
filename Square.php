<?php

require_once 'ShapeAbstract.php';
class Square extends ShapeAbstract
{

    public function __construct(private $sideLength)
    {
    }

    public function calculateArea(): float|int
    {
        return $this->sideLength * $this->sideLength;
    }
}